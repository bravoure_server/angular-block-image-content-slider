# Bravoure - Block Image Content Slider

## Directive: Code to be added:

        <block-image-content-slider></block-image-content-slider>

## Use

It can be added in the cms under the option of "blocks", and inserting the block image,


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-image-content-slider": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
