(function () {
    'use strict';

    function blockImageContentSlider () {
        return {
            restrict: 'E',
            replace: false,

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                // defines the type of list
                $scope.module.kind = 'image_content_slide';
            },

            template: '<block-list-loader></block-list-loader>'
        }
    }

    blockImageContentSlider.$inject = ['PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .directive('blockImageContentSlider', blockImageContentSlider);

})();
